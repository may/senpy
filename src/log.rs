use colored::Colorize;

pub fn info(text: &str) {
    println!("{} {}", "INFO".yellow().bold(), text);
}
pub fn success(text: &str) {
    println!("{} {}", "SUCCESS".green().bold(), text);
}
pub fn error(text: &str) {
    println!("{} {}", "ERROR".red().bold(), text);
}
