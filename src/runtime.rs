use std::{collections::HashMap, error::Error, fmt};

use egui::{FontData, FontDefinitions, FontFamily};
use macroquad::{
    audio::{
        load_sound_from_bytes, play_sound, play_sound_once, stop_sound, PlaySoundParams, Sound,
    },
    texture::Texture2D,
    window::{screen_height, screen_width},
};
use pollster::FutureExt;
use serde::{Deserialize, Serialize};

use crate::compiler::Instruction;

#[derive(Debug)]
pub enum RuntimeError {
    StackEmpty,
    InvalidTypes,
    UnknownFunction(String),
    ChoiceOutOfBounds,
    PythonError(String),
    InconvertibleNumber,
}

impl Error for RuntimeError {}
impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RuntimeError::StackEmpty => {
                write!(f, "RuntimeError: Couldn't pop due to empty stack")
            }
            RuntimeError::InvalidTypes => {
                write!(f, "RuntimeError: Types for operation were incorrect")
            }
            RuntimeError::UnknownFunction(func) => {
                write!(
                    f,
                    "RuntimeError: Attempted to call function that doesn't exist: {func}",
                )
            }
            RuntimeError::ChoiceOutOfBounds => {
                write!(f, "RuntimeError: Menu choice was not present in options")
            }
            RuntimeError::PythonError(err) => {
                writeln!(f, "RuntimeError: Python execution failed")?;
                write!(f, "{err}")
            }
            RuntimeError::InconvertibleNumber => {
                write!(f, "RuntimeError: Number was not a valid integer or float")
            }
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Operation {
    Say,
    Call,
    Define,
    Input,
    Store,
    Get,
    Menu,
    Select,
    Option,
    SaveStackLength,
    Execute,
    Case,
    Switch,
    SetFont,
    SetTexture,
    ShowTexture,
    SetBackground,
    ClearTextures,
    Pixel,
    Percent,
    Rect,
    GetVariable,
    PlaySound,
    SetSound,
    LoopSound,
    StopSound,
}

#[derive(Debug, Clone)]
pub enum OutputEvent {
    Text(String, Vec<String>),
    Menu(Vec<String>),
    InputRequested,
}

#[derive(Debug)]
pub enum InputEvent {
    Confirm,
    MenuChoice(usize),
    Input(String),
}

#[derive(Debug, Clone, Copy)]
pub enum Location {
    Pixel(f32),
    Percent(f32),
}

#[derive(Debug, Clone)]
pub struct Rect {
    x: Location,
    y: Location,
    width: Location,
}

impl Rect {
    pub fn resolve_x(&self) -> f32 {
        match self.x {
            Location::Pixel(pixel) => pixel,
            Location::Percent(percent) => screen_width() * percent / 100.0,
        }
    }
    pub fn resolve_y(&self) -> f32 {
        match self.y {
            Location::Pixel(pixel) => pixel,
            Location::Percent(percent) => screen_height() * percent / 100.0,
        }
    }
    pub fn resolve_width(&self) -> f32 {
        match self.width {
            Location::Pixel(pixel) => pixel,
            Location::Percent(percent) => screen_width() * percent / 100.0,
        }
    }
}

#[derive(Debug, Clone)]
pub enum Value {
    String(String),
    Code(String),
    Reference(usize),
    Integer(i64),
    Float(f64),
    Array(Vec<Value>),
    Bool(bool),
    Bytes(Vec<u8>),
}

pub struct Runtime {
    instructions: Vec<Instruction>,
    stack: Vec<Value>,
    call_stack: Vec<usize>,
    option_stack: Vec<usize>,
    case_stack: Vec<(usize, bool)>,
    location_stack: Vec<Location>,
    rect_stack: Vec<Rect>,
    pub displayed_textures: Vec<(String, Rect)>,
    functions: HashMap<String, usize>,
    variables: HashMap<String, Value>,
    pub textures: HashMap<String, Texture2D>,
    sounds: HashMap<String, Sound>,
    pc: usize,
    definition_level: usize,
    stack_length: usize,
    pub background: Option<String>,
}

macro_rules! operation {
    ($obj:expr, |$var_name:ident: $var_type:ident| $result:expr) => {{
        let $var_name = $obj.pop()?;
        if let Value::$var_type($var_name) = $var_name {
            Ok($result)
        } else {
            Err(RuntimeError::InvalidTypes)
        }
    }};
    ($obj:expr, |$vn1:ident: $vt1:ident, $vn2:ident: $vt2:ident| $result:expr) => {{
        let $vn1 = $obj.pop()?;
        let $vn2 = $obj.pop()?;
        if let (Value::$vt1($vn1), Value::$vt2($vn2)) = ($vn1, $vn2) {
            Ok($result)
        } else {
            Err(RuntimeError::InvalidTypes)
        }
    }};
    ($obj:expr, |$var_name:ident: $var_type:ident| $result:expr, |$var_name_2:ident: $var_type_2:ident| $result_2:expr) => {{
        let $var_name = $obj.pop()?;
        if let Value::$var_type($var_name) = $var_name {
            Ok($result)
        } else if let Value::$var_type_2($var_name_2) = $var_name {
            Ok($result_2)
        } else {
            Err(RuntimeError::InvalidTypes)
        }
    }};
}

impl Runtime {
    pub fn new(instructions: Vec<Instruction>) -> Self {
        Self {
            instructions,
            pc: 0,
            call_stack: vec![],
            stack: vec![],
            functions: HashMap::new(),
            definition_level: 0,
            variables: HashMap::new(),
            stack_length: 0,
            option_stack: vec![],
            case_stack: vec![],
            textures: HashMap::new(),
            displayed_textures: vec![],
            location_stack: vec![],
            rect_stack: vec![],
            background: None,
            sounds: HashMap::new(),
        }
    }

    pub fn poll(&mut self, data: &InputEvent) -> Result<Option<OutputEvent>, RuntimeError> {
        loop {
            if self.pc == self.instructions.len() {
                return Ok(None);
            }
            let instruction = self.instructions[self.pc].clone();
            let response = self.execute_instruction(instruction, data)?;
            self.pc += 1;
            if response.is_some() {
                return Ok(response);
            }
        }
    }

    fn execute_operation(
        &mut self,
        operation: Operation,
        data: &InputEvent,
    ) -> Result<Option<OutputEvent>, RuntimeError> {
        match operation {
            Operation::PlaySound => {
                operation!(self, |name: String| {
                    let sound = self.sounds.get(&name).unwrap();
                    play_sound_once(*sound);
                    None
                })
            }
            Operation::LoopSound => {
                operation!(self, |name: String| {
                    let sound = self.sounds.get(&name).unwrap();
                    play_sound(
                        *sound,
                        PlaySoundParams {
                            looped: true,
                            ..Default::default()
                        },
                    );
                    None
                })
            }
            Operation::StopSound => {
                operation!(self, |name: String| {
                    let sound = self.sounds.get(&name).unwrap();
                    stop_sound(*sound);
                    None
                })
            }
            Operation::SetBackground => {
                operation!(self, |name: String| {
                    self.background = Some(name);
                    None
                })
            }
            Operation::Pixel => {
                operation!(
                    self,
                    |as_int: Integer| {
                        self.location_stack.push(Location::Pixel(as_int as f32));
                        None
                    },
                    |as_float: Float| {
                        self.location_stack.push(Location::Pixel(as_float as f32));
                        None
                    }
                )
            }
            Operation::Percent => {
                operation!(
                    self,
                    |as_int: Integer| {
                        self.location_stack.push(Location::Percent(as_int as f32));
                        None
                    },
                    |as_float: Float| {
                        self.location_stack.push(Location::Percent(as_float as f32));
                        None
                    }
                )
            }
            Operation::Rect => {
                let width = self.location_stack.pop().unwrap();
                let y = self.location_stack.pop().unwrap();
                let x = self.location_stack.pop().unwrap();
                self.rect_stack.push(Rect { x, y, width });
                Ok(None)
            }
            Operation::GetVariable => {
                operation!(self, |name: String| {
                    let value = self.variables.get(&name).unwrap();
                    self.stack.push(value.clone());
                    None
                })
            }
            Operation::SetFont => {
                operation!(self, |data: Bytes| {
                    egui_macroquad::cfg(|ctx| {
                        let mut fonts = FontDefinitions::default();
                        fonts
                            .font_data
                            .insert("main_font".to_string(), FontData::from_owned(data));
                        fonts
                            .families
                            .get_mut(&FontFamily::Proportional)
                            .unwrap()
                            .insert(0, "main_font".to_string());
                        fonts
                            .families
                            .get_mut(&FontFamily::Monospace)
                            .unwrap()
                            .push("main_font".to_string());
                        ctx.set_fonts(fonts);
                    });
                    None
                })
            }
            Operation::SetTexture => {
                operation!(self, |name: String, data: Bytes| {
                    let tex = Texture2D::from_file_with_format(&data, None);
                    self.textures.insert(name, tex);
                    None
                })
            }
            Operation::SetSound => {
                operation!(self, |name: String, data: Bytes| {
                    let sound = load_sound_from_bytes(&data).block_on().unwrap();
                    self.sounds.insert(name, sound);
                    None
                })
            }
            Operation::ShowTexture => {
                let rect = self.rect_stack.pop().unwrap();
                operation!(self, |name: String| {
                    self.displayed_textures.push((name, rect));
                    None
                })
            }
            Operation::ClearTextures => {
                self.displayed_textures.clear();
                Ok(None)
            }
            Operation::Define => {
                operation!(self, |name: String, reference: Reference| {
                    self.functions.insert(name, reference);
                    None
                })
            }
            Operation::Say => {
                operation!(self, |text: String, speaker: String| {
                    let text = crate::python::template_string(&text, &self.variables)?;
                    Some(OutputEvent::Text(speaker, vec![text]))
                })
            }
            Operation::Call => {
                operation!(
                    self,
                    |name: String| {
                        if let Some(reference) = self.functions.get(&name) {
                            self.call_stack.push(self.pc);
                            self.pc = *reference - 1;
                            None
                        } else {
                            return Err(RuntimeError::UnknownFunction(name));
                        }
                    },
                    |reference: Reference| {
                        self.call_stack.push(self.pc);
                        self.pc = reference - 1;
                        None
                    }
                )
            }
            Operation::Input => Ok(Some(OutputEvent::InputRequested)),
            Operation::Store => {
                operation!(self, |name: String| {
                    let value = self.pop()?;
                    self.variables.insert(name, value);
                    None
                })
            }
            Operation::Get => {
                match data {
                    InputEvent::Input(text) => self.stack.push(Value::String(text.clone())),
                    InputEvent::MenuChoice(index) => self.stack.push(Value::Integer(*index as i64)),
                    InputEvent::Confirm => {}
                }
                Ok(None)
            }
            Operation::SaveStackLength => {
                self.stack_length = self.stack.len();
                Ok(None)
            }
            Operation::Menu => {
                let changes = self.stack.len() + 1 - self.stack_length;
                let mut options = vec![];
                for _ in 0..changes {
                    if let Value::String(text) = self.pop()? {
                        options.push(text);
                    }
                }
                Ok(Some(OutputEvent::Menu(
                    options.iter().rev().cloned().collect(),
                )))
            }
            Operation::Option => {
                operation!(self, |reference: Reference| {
                    self.option_stack.push(reference);
                    None
                })
            }
            Operation::Select => {
                operation!(self, |index: Integer| {
                    let chosen_option = self
                        .option_stack
                        .get(index as usize)
                        .ok_or(RuntimeError::ChoiceOutOfBounds)?;
                    self.call_stack.push(self.pc);
                    self.pc = chosen_option - 1;
                    self.option_stack.clear();
                    None
                })
            }

            Operation::Execute => {
                operation!(self, |code: Code| {
                    crate::python::execute_code(&code, &mut self.variables)?;
                    None
                })
            }
            Operation::Case => {
                operation!(self, |condition: Code, reference: Reference| {
                    let value = crate::python::evaluate_expression(&condition, &self.variables)?;
                    if let Value::Bool(value) = value {
                        self.case_stack.push((reference, value));
                        None
                    } else {
                        return Err(RuntimeError::InvalidTypes);
                    }
                })
            }
            Operation::Switch => {
                'switch: for (reference, value) in &self.case_stack {
                    if *value {
                        self.call_stack.push(self.pc);
                        self.pc = reference - 1;
                        break 'switch;
                    }
                }
                self.case_stack.clear();
                Ok(None)
            }
        }
    }

    fn pop(&mut self) -> Result<Value, RuntimeError> {
        self.stack.pop().ok_or(RuntimeError::StackEmpty)
    }

    fn execute_instruction(
        &mut self,
        instruction: Instruction,
        data: &InputEvent,
    ) -> Result<Option<OutputEvent>, RuntimeError> {
        match instruction {
            Instruction::PushString(text) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::String(text));
                }
                Ok(None)
            }
            Instruction::PushCode(text) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::Code(text));
                }
                Ok(None)
            }
            Instruction::PushInt(number) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::Integer(number));
                }
                Ok(None)
            }
            Instruction::PushFloat(number) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::Float(number));
                }
                Ok(None)
            }
            Instruction::FunctionReference(index) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::Reference(index));
                }
                Ok(None)
            }
            Instruction::StartDefine => {
                self.definition_level += 1;
                Ok(None)
            }
            Instruction::Return => {
                if self.definition_level == 0 {
                    let destination = self.call_stack.pop().unwrap();
                    self.pc = destination;
                } else {
                    self.definition_level -= 1;
                }
                Ok(None)
            }
            Instruction::ExecuteOperation(operation) => {
                if self.definition_level == 0 {
                    self.execute_operation(operation, data)
                } else {
                    Ok(None)
                }
            }
            Instruction::PushBytes(bytes) => {
                if self.definition_level == 0 {
                    self.stack.push(Value::Bytes(bytes));
                }
                Ok(None)
            }
        }
    }
}
