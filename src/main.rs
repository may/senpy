use clap::{Parser, Subcommand};
use colored::Colorize;
use std::{
    error::Error,
    fs,
    io::Write,
    path::{Path, PathBuf},
};

mod compiler;
mod gui;
mod lexer;
mod log;
mod parser;
mod python;
mod runtime;

use compiler::Instruction;
use log::{error, info, success};

async fn execute(source: impl AsRef<Path>) -> Result<(), Box<dyn Error>> {
    let text = fs::read(&source)?;
    let instructions: Vec<Instruction> = bincode::deserialize(&text[..])?;
    success(&format!(
        "Deserialized instructions from binary `{}`",
        source.as_ref().to_str().unwrap().blue()
    ));
    gui::run(instructions).await?;
    Ok(())
}

async fn run(source: impl AsRef<Path>) -> Result<(), Box<dyn Error>> {
    let text = fs::read_to_string(&source)?;
    let text = text.trim();
    let tokens = lexer::lex(text)?;
    let nodes = parser::parse(&tokens)?;
    let instructions = compiler::compile(&nodes)?;
    success(&format!(
        "Compiled script from `{}`",
        source.as_ref().to_str().unwrap().blue()
    ));
    gui::run(instructions).await?;
    Ok(())
}

fn compile(source: impl AsRef<Path>, output: impl AsRef<Path>) -> Result<(), Box<dyn Error>> {
    info(&format!(
        "Compiling `{}` to `{}`",
        source.as_ref().to_str().unwrap(),
        output.as_ref().to_str().unwrap()
    ));
    let text = fs::read_to_string(source)?;
    let text = text.trim();
    let tokens = lexer::lex(text)?;
    let nodes = parser::parse(&tokens)?;
    let instructions = compiler::compile(&nodes)?;
    let bytes = bincode::serialize(&instructions)?;
    let mut file = fs::File::create(output)?;
    file.write_all(&bytes)?;
    Ok(())
}

async fn start() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    match args.command {
        Command::Compile { source, output } => {
            let source = PathBuf::from(source);
            let output = if let Some(path) = output {
                PathBuf::from(path)
            } else {
                let mut path = source.clone();
                path.set_extension("sp");
                path
            };
            compile(source, output)?;
        }
        Command::Run { source } => {
            let source = PathBuf::from(source);
            run(source).await?;
        }
        Command::Execute { source } => {
            let source = PathBuf::from(source);
            execute(source).await?;
        }
    }
    Ok(())
}

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    Compile {
        #[arg(short, long)]
        source: String,
        #[arg(short, long)]
        output: Option<String>,
    },
    Run {
        #[arg(short, long)]
        source: String,
    },
    Execute {
        #[arg(short, long)]
        source: String,
    },
}

#[macroquad::main("senpy")]
async fn main() {
    match start().await {
        Ok(_) => {}
        Err(e) => error(&format!("{e}")),
    }
}
