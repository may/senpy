use std::{error::Error, fmt, str::Chars};

#[derive(Debug)]
pub enum LexError {
    UnclosedQuotation(usize, usize),
    UnclosedCode(usize, usize),
    ParseNumError(String),
}

impl Error for LexError {}
impl fmt::Display for LexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LexError::UnclosedQuotation(row, col) => {
                write!(f, "LexError: Unclosed string at [{row}:{col}]")
            }
            LexError::UnclosedCode(row, col) => {
                write!(f, "LexError: Unclosed code block at [{row}:{col}]")
            }
            LexError::ParseNumError(text) => {
                write!(f, "LexError: Failed to parse number {}", text)
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Token {
    OpenBracket,
    CloseBracket,
    String(String),
    Integer(i64),
    Float(f64),
    Code(String),
    Operation(String),
    Macro(String),
    Asset(String),
}

fn advance_chars(chars: &mut Chars, row: &mut usize, col: &mut usize) -> Option<char> {
    let result = chars.next();
    *col += 1;
    if let Some('\n') = result {
        *row += 1;
        *col = 0;
    }
    result
}

pub fn lex(text: &str) -> Result<Vec<Token>, LexError> {
    let mut tokens = vec![];
    let mut chars = text.chars();
    let mut previous_char = ' ';
    let mut row = 0;
    let mut col = 0;
    let mut reuse_flag = false;
    let mut reuse_char = ' ';

    while let Some(chr) = if reuse_flag {
        reuse_flag = false;
        Some(reuse_char)
    } else {
        advance_chars(&mut chars, &mut row, &mut col)
    } {
        match chr {
            '[' => tokens.push(Token::OpenBracket),
            ']' => tokens.push(Token::CloseBracket),
            '"' => {
                let (start_row, start_col) = (row, col);
                let mut builder = String::new();
                let mut previous_char = chr;
                let mut current_char = advance_chars(&mut chars, &mut row, &mut col)
                    .ok_or(LexError::UnclosedQuotation(start_row, start_col - 1))?;
                while current_char != '"' || previous_char == '\\' {
                    if current_char != '\\' || previous_char == '\\' {
                        builder.push(current_char);
                        previous_char = ' ';
                    } else {
                        previous_char = current_char;
                    }
                    current_char = advance_chars(&mut chars, &mut row, &mut col)
                        .ok_or(LexError::UnclosedQuotation(start_row, start_col - 1))?;
                }
                tokens.push(Token::String(builder));
            }
            '`' => {
                let (start_row, start_col) = (row, col);
                let mut builder = String::new();
                let mut current_char = advance_chars(&mut chars, &mut row, &mut col)
                    .ok_or(LexError::UnclosedCode(start_row, start_col - 1))?;
                while current_char != '`' {
                    builder.push(current_char);
                    current_char = advance_chars(&mut chars, &mut row, &mut col)
                        .ok_or(LexError::UnclosedCode(start_row, start_col - 1))?;
                }
                tokens.push(Token::Code(builder));
            }
            '\t' | '\n' | ' ' => {}
            '/' => {
                if previous_char == '/' {
                    while let Some(next_char) = advance_chars(&mut chars, &mut row, &mut col) {
                        if next_char == '\n' {
                            break;
                        }
                    }
                }
            }
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                let mut builder = String::new();
                let mut current_char = chr;
                while current_char.is_ascii_digit() || current_char == '.' {
                    builder.push(current_char);
                    current_char = advance_chars(&mut chars, &mut row, &mut col).unwrap();
                }
                if let Ok(as_int) = builder.parse::<i64>() {
                    tokens.push(Token::Integer(as_int));
                } else if let Ok(as_float) = builder.parse::<f64>() {
                    tokens.push(Token::Float(as_float));
                } else {
                    return Err(LexError::ParseNumError(builder.clone()));
                }
                reuse_flag = true;
                reuse_char = current_char;
            }
            _ => {
                let mut builder = String::from(chr);
                'builder: loop {
                    match advance_chars(&mut chars, &mut row, &mut col) {
                        Some('\t' | '\n' | ' ') => break 'builder,
                        Some(next_char) => builder.push(next_char),
                        _ => break 'builder,
                    }
                }
                let mut builder_chars = builder.chars();
                let first_char = builder_chars.next();
                if let Some('#') = first_char {
                    tokens.push(Token::Macro(builder_chars.collect()));
                } else if let Some('+') = first_char {
                    tokens.push(Token::Asset(builder_chars.collect()));
                } else {
                    tokens.push(Token::Operation(builder));
                }
            }
        }
        previous_char = chr;
    }
    Ok(tokens)
}
