use std::{error::Error, fmt};

use crate::compiler::Macro;
use crate::lexer::Token;
use crate::runtime::Operation;

#[derive(Debug)]
pub enum ParseError {
    InvalidOperation(String),
    InvalidMacro(String),
}

impl Error for ParseError {}
impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseError::InvalidOperation(operation) => {
                write!(f, "ParseError: Unknown operation {operation}")
            }
            ParseError::InvalidMacro(text) => {
                write!(f, "ParseError: Unknown macro {text}")
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum Node {
    Block(Vec<Node>),
    String(String),
    Integer(i64),
    Float(f64),
    Code(String),
    Operation(Operation),
    Macro(Macro),
    Asset(String),
}

fn parse_operation(text: &str) -> Result<Operation, ParseError> {
    match text {
        "!" => Ok(Operation::Say),
        "def" => Ok(Operation::Define),
        "call" => Ok(Operation::Call),
        "input" => Ok(Operation::Input),
        "store" => Ok(Operation::Store),
        "get" => Ok(Operation::Get),
        "menu" => Ok(Operation::Menu),
        "select" => Ok(Operation::Select),
        "|" => Ok(Operation::Option),
        "save_stack_length" => Ok(Operation::SaveStackLength),
        "x" => Ok(Operation::Execute),
        "@" | "case" => Ok(Operation::Case),
        "$" => Ok(Operation::GetVariable),
        "switch" => Ok(Operation::Switch),
        "show" => Ok(Operation::ShowTexture),
        "play-once" => Ok(Operation::PlaySound),
        "play-looping" => Ok(Operation::LoopSound),
        "stop-sound" => Ok(Operation::StopSound),
        "background" => Ok(Operation::SetBackground),
        "clear" => Ok(Operation::ClearTextures),
        "px" => Ok(Operation::Pixel),
        "%" => Ok(Operation::Percent),
        "Rect" => Ok(Operation::Rect),
        "FONT" => Ok(Operation::SetFont),
        "TEXTURE" => Ok(Operation::SetTexture),
        "SOUND" => Ok(Operation::SetSound),
        _ => Err(ParseError::InvalidOperation(text.to_string())),
    }
}

fn parse_macro(text: &str) -> Result<Macro, ParseError> {
    match text {
        "include" => Ok(Macro::Include),
        "menu" => Ok(Macro::Menu),
        _ => Err(ParseError::InvalidMacro(text.to_string())),
    }
}

pub fn parse(tokens: &[Token]) -> Result<Vec<Node>, ParseError> {
    let mut result = vec![]; // Top-level tokens converted to node
    let mut builder = vec![]; // Non top-level tokens stored as tokens
    let mut level = 0; // Current indentation level

    for token in tokens {
        match token {
            Token::OpenBracket => {
                level += 1;
                if level != 1 {
                    builder.push(token.clone());
                }
            }
            Token::CloseBracket => {
                level -= 1;
                if level == 0 {
                    let inner_node = parse(&builder)?;
                    result.push(Node::Block(inner_node));
                    builder.clear();
                } else {
                    builder.push(token.clone());
                }
            }
            Token::String(text) => {
                if level == 0 {
                    result.push(Node::String(text.clone()));
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Integer(number) => {
                if level == 0 {
                    result.push(Node::Integer(*number));
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Float(number) => {
                if level == 0 {
                    result.push(Node::Float(*number));
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Code(text) => {
                if level == 0 {
                    result.push(Node::Code(text.clone()));
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Operation(text) => {
                if level == 0 {
                    if let Ok(operation) = parse_operation(text) {
                        result.push(Node::Operation(operation));
                    } else {
                        result.push(Node::String(text.clone()));
                        result.push(Node::Operation(Operation::Call));
                    }
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Macro(text) => {
                if level == 0 {
                    result.push(Node::Macro(parse_macro(text)?));
                } else {
                    builder.push(token.clone());
                }
            }
            Token::Asset(path) => {
                if level == 0 {
                    result.push(Node::Asset(path.clone()));
                } else {
                    builder.push(token.clone());
                }
            }
        }
    }

    Ok(result)
}
