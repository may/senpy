use std::{error::Error, fmt, fs};

use serde::{Deserialize, Serialize};

use crate::{parser::Node, runtime::Operation};

#[derive(Debug)]
pub enum CompileError {
    InternalError,
    MissingFile(String),
}

impl Error for CompileError {}
impl fmt::Display for CompileError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CompileError::InternalError => {
                write!(
                    f,
                    "CompileError: Internal error. Likely a bug in the compiler."
                )
            }
            CompileError::MissingFile(path) => {
                write!(f, "Failed to read path: {}.", path)
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Instruction {
    PushString(String),
    PushCode(String),
    PushBytes(Vec<u8>),
    PushInt(i64),
    PushFloat(f64),
    ExecuteOperation(Operation),
    FunctionReference(usize),
    StartDefine,
    Return,
}

#[derive(Debug, Clone)]
pub enum Macro {
    Include,
    Menu,
}

pub struct Compiler {
    result: Vec<Instruction>,
    function_stack: Vec<usize>,
}

impl Compiler {
    pub fn new() -> Self {
        Compiler {
            result: vec![],
            function_stack: vec![],
        }
    }
    pub fn compile(&mut self, nodes: &[Node]) -> Result<(), CompileError> {
        for node in nodes {
            match node {
                Node::String(text) => self.result.push(Instruction::PushString(text.clone())),
                Node::Code(text) => self.result.push(Instruction::PushCode(text.clone())),
                Node::Integer(number) => self.result.push(Instruction::PushInt(*number)),
                Node::Float(number) => self.result.push(Instruction::PushFloat(*number)),
                Node::Operation(operation) => {
                    self.result.push(Instruction::ExecuteOperation(*operation));
                }
                Node::Asset(path) => {
                    let data =
                        fs::read(&path).map_err(|_| CompileError::MissingFile(path.clone()))?;
                    self.result.push(Instruction::PushBytes(data));
                }
                Node::Block(nodes) => {
                    self.result.push(Instruction::StartDefine);
                    self.function_stack.push(self.result.len());
                    self.compile(nodes)?;
                    self.result.push(Instruction::Return);
                    let location = self
                        .function_stack
                        .pop()
                        .ok_or(CompileError::InternalError)?;
                    self.result.push(Instruction::FunctionReference(location));
                }
                Node::Macro(mcr) => match mcr {
                    Macro::Include => {
                        let source = self.result.pop().unwrap();
                        if let Instruction::PushString(source) = source {
                            let text = fs::read_to_string(source).unwrap();
                            let tokens = crate::lexer::lex(text.trim()).unwrap();
                            let nodes = crate::parser::parse(&tokens).unwrap();
                            self.compile(&nodes)?;
                        }
                    }
                    Macro::Menu => {
                        self.result
                            .push(Instruction::ExecuteOperation(Operation::SaveStackLength));
                        self.result
                            .push(Instruction::ExecuteOperation(Operation::Call));
                        self.result
                            .push(Instruction::ExecuteOperation(Operation::Menu));
                        self.result
                            .push(Instruction::ExecuteOperation(Operation::Get));
                    }
                },
            }
        }
        Ok(())
    }
}

pub fn compile(nodes: &[Node]) -> Result<Vec<Instruction>, CompileError> {
    let mut compiler = Compiler::new();
    compiler.compile(nodes)?;
    Ok(compiler.result)
}
