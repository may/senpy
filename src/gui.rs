use crate::log::info;
use crate::{
    compiler::Instruction,
    runtime::{InputEvent, OutputEvent, Runtime, RuntimeError},
};
use egui::{Button, Color32, Frame, RichText, Rounding};
use macroquad::prelude::*;
use std::error::Error;

struct Game {
    runtime: Runtime,
    last_event: OutputEvent,
    speaker: String,
    text: String,
    menu_options: Vec<String>,
    data: String,
}

impl Game {
    fn new(instructions: Vec<Instruction>) -> Self {
        let runtime = Runtime::new(instructions);
        let last_event = OutputEvent::Text(String::new(), vec![]);
        let speaker = String::from("Welcome to senpy");
        let text = String::from("Press <space> to begin");
        let menu_options = vec![];
        let data = String::new();
        Self {
            runtime,
            last_event,
            speaker,
            text,
            menu_options,
            data,
        }
    }

    fn draw_textures(&mut self) {
        if let Some(texture_name) = &self.runtime.background {
            let texture = self.runtime.textures.get(texture_name).unwrap();
            let height = screen_height();
            let actual_height = texture.height();
            let scale_factor = height / actual_height;
            let width = texture.width() * scale_factor;
            draw_texture_ex(
                *texture,
                (screen_width() - width) / 2.0,
                0.0,
                WHITE,
                DrawTextureParams {
                    dest_size: Some(Vec2::new(width, height)),
                    ..Default::default()
                },
            );
        }
        for (texture_name, rect) in &self.runtime.displayed_textures {
            let texture = self.runtime.textures.get(texture_name).unwrap();
            let middle_x = rect.resolve_x();
            let middle_y = rect.resolve_y();
            let width = rect.resolve_width();
            let actual_width = texture.width();
            let scale_factor = width / actual_width;
            let height = texture.height() * scale_factor;
            draw_texture_ex(
                *texture,
                middle_x - width / 2.0,
                middle_y - height / 2.0,
                WHITE,
                DrawTextureParams {
                    dest_size: Some(Vec2::new(width, height)),
                    ..Default::default()
                },
            );
        }
    }

    fn draw_ui(&mut self) {
        egui_macroquad::ui(|ctx| {
            // Draw chat box
            egui::TopBottomPanel::bottom("chat")
                .min_height(100.0)
                .frame(Frame::none())
                .show_separator_line(false)
                .show(ctx, |ui| {
                    ui.vertical_centered(|ui| {
                        ui.label(
                            RichText::new(&self.speaker)
                                .size(20.0)
                                .color(Color32::from_rgb(255, 255, 255)),
                        );
                        ui.label(
                            RichText::new(&self.text)
                                .size(13.0)
                                .color(Color32::from_rgb(255, 255, 255)),
                        );

                        // Draw options menu
                        if let OutputEvent::Menu(_) = self.last_event {
                            let mut click = false;
                            let mut index = 0;
                            for (i, option) in self.menu_options.iter().enumerate() {
                                let button = ui.add(
                                    Button::new(option)
                                        .fill(Color32::TRANSPARENT)
                                        .rounding(Rounding::same(0.0))
                                        .min_size(egui::Vec2::new(120.0, 22.0)),
                                );
                                if button.clicked() {
                                    index = i;
                                    click = true;
                                }
                            }
                            if click {
                                self.advance_event(&InputEvent::MenuChoice(index)).unwrap();
                            }
                        }

                        // Draw text input
                        if let OutputEvent::InputRequested = self.last_event {
                            ui.add(egui::TextEdit::singleline(&mut self.data));
                        }
                    });
                });
        });
    }

    fn handle_input(&mut self) -> Result<bool, RuntimeError> {
        if is_key_pressed(KeyCode::Escape) {
            return Ok(true);
        } else if is_key_pressed(KeyCode::Space)
            && matches!(self.last_event, OutputEvent::Text(_, _))
        {
            self.advance_event(&InputEvent::Confirm)?;
        } else if is_key_pressed(KeyCode::Enter)
            && matches!(self.last_event, OutputEvent::InputRequested)
        {
            self.advance_event(&InputEvent::Input(self.data.clone()))?;
            self.data.clear();
        }
        Ok(false)
    }

    fn advance_event(&mut self, event: &InputEvent) -> Result<(), RuntimeError> {
        if let Some(event) = self.runtime.poll(event)? {
            match event.clone() {
                OutputEvent::Text(character, lines) => {
                    self.speaker = character;
                    self.text = lines.join("\n");
                }
                OutputEvent::InputRequested => {}
                OutputEvent::Menu(options) => {
                    self.menu_options = options;
                }
            }
            self.last_event = event;
        }
        Ok(())
    }
}

pub async fn run(instructions: Vec<Instruction>) -> Result<(), Box<dyn Error>> {
    let mut game = Game::new(instructions);
    info("Starting GUI");
    egui_macroquad::cfg(|ctx| {
        ctx.set_pixels_per_point(2.0);
    });
    'main: loop {
        clear_background(BLACK);
        game.draw_textures();
        game.draw_ui();
        if game.handle_input()? {
            break 'main;
        };
        egui_macroquad::draw();
        next_frame().await;
    }
    Ok(())
}
