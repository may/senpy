use crate::runtime::{RuntimeError, Value};
use std::{collections::HashMap, fs, io::Write, process};

const TEMP_PATH: &str = "/tmp/pyruntime";

fn pyrepr(value: &Value) -> String {
    match value {
        Value::String(text) | Value::Code(text) => format!("\"{}\"", text.escape_debug()),
        Value::Reference(reference) => reference.to_string(),
        Value::Integer(number) => number.to_string(),
        Value::Float(number) => number.to_string(),
        Value::Bool(value) => {
            if *value {
                "True".to_string()
            } else {
                "False".to_string()
            }
        }
        Value::Array(data) => format!(
            "[{}]",
            data.iter().map(pyrepr).collect::<Vec<_>>().join(",")
        ),
        Value::Bytes(data) => format!(
            "[{}]",
            data.iter()
                .map(|x| x.to_string())
                .collect::<Vec<_>>()
                .join(", ")
        ),
    }
}

fn evaluate(text: &str) -> Result<String, RuntimeError> {
    let mut file = fs::File::create(TEMP_PATH).unwrap();
    write!(file, "{text}").unwrap();

    let output = process::Command::new("python")
        .arg(TEMP_PATH)
        .output()
        .unwrap();
    if output.status.success() {
        Ok(String::from_utf8_lossy(&output.stdout).trim().to_string())
    } else {
        Err(RuntimeError::PythonError(
            String::from_utf8_lossy(&output.stderr).to_string(),
        ))
    }
}

fn insert_variables(text: &mut String, variables: &HashMap<String, Value>) {
    for (key, value) in variables.iter() {
        text.push_str(&format!("\n{} = {}\n", key, pyrepr(value)));
    }
}

fn convert_value(value: serde_json::Value) -> Result<Value, RuntimeError> {
    match value {
        serde_json::Value::String(text) => Ok(Value::String(text)),
        serde_json::Value::Number(number) => {
            if let Some(num_i64) = number.as_i64() {
                Ok(Value::Integer(num_i64))
            } else if let Some(num_f64) = number.as_f64() {
                Ok(Value::Float(num_f64))
            } else {
                Err(RuntimeError::InconvertibleNumber)
            }
        }
        serde_json::Value::Bool(value) => Ok(Value::Bool(value)),
        serde_json::Value::Array(data) => {
            let values = data
                .iter()
                .map(|x| convert_value(x.clone()))
                .collect::<Result<Vec<_>, RuntimeError>>()?;
            Ok(Value::Array(values))
        }
        _ => panic!("Unsupported"),
    }
}

pub fn template_string(
    text: &str,
    variables: &HashMap<String, Value>,
) -> Result<String, RuntimeError> {
    let mut code = String::new();
    insert_variables(&mut code, variables);
    code.push_str(&format!("\nprint(f\"{text}\")\n"));
    evaluate(&code)
}

pub fn evaluate_expression(
    text: &str,
    variables: &HashMap<String, Value>,
) -> Result<Value, RuntimeError> {
    let mut code = String::new();
    insert_variables(&mut code, variables);
    code.push_str(&format!("\nimport json\nprint(json.dumps({text}))\n"));
    let response = evaluate(&code)?;
    let data = serde_json::from_str(&response).unwrap();
    convert_value(data)
}

pub fn execute_code(
    text: &str,
    variables: &mut HashMap<String, Value>,
) -> Result<(), RuntimeError> {
    let mut code = String::new();
    insert_variables(&mut code, variables);

    let mut whitespace = 0;
    'counter: for chr in text.chars() {
        if matches!(chr, '\t' | '\n' | ' ') {
            whitespace += 1;
        } else {
            break 'counter;
        }
    }
    let text = text
        .split('\n')
        .filter(|x| !x.trim().is_empty())
        .map(|x| {
            if whitespace == 0 {
                x
            } else {
                &x[whitespace - 1..x.len()]
            }
        })
        .collect::<Vec<_>>()
        .join("\n");
    code.push_str(&format!("\n{text}\n"));
    code.push_str(
        "\nvars = {key: value for key, value in locals().items() if not key.startswith('_') and type(value) in [int, float, str, bool, list]}\n",
    );
    code.push_str("\nimport json\n");
    code.push_str("\nprint(json.dumps(vars))\n");
    let result = evaluate(&code)?;
    let data: serde_json::Value = serde_json::from_str(&result).unwrap();
    if let serde_json::Value::Object(map) = data {
        for (key, value) in map.iter() {
            let value = convert_value(value.clone())?;
            variables.insert(key.clone(), value);
        }
    }

    Ok(())
}
